#!/usr/bin/env python

import argparse
import community
import csv
import datetime
import json
import networkx as nx
from collections import Counter
from dateutil.parser import parse

class TweetFilter():
    def accepts(self, tweet):
        '''Checks if the filter lets the tweet pass or not'''
        pass

class HashTagFilter(TweetFilter):
    def __init__(self, hashtags=[], exclude=False, exclusive=True):
        self.exclude = exclude
        self.exclusive = exclusive
        if hashtags:
            self.hashtags = [x.lower() for x in hashtags]
        else:
            self.hashtags = []

    def accepts(self, tweet):
        hashtags = tweet.hashtags()

        if self.exclude:
            # Exclude when any of the specified hashtags matches
            result = any(elem in hashtags for elem in self.hashtags)
            # We exclude anything that matches
            result = not result
        else:
            if self.exclusive:
                result = all(elem in hashtags for elem in self.hashtags)
            else:
                result = any(elem in hashtags for elem in self.hashtags)
        return result

    def __repr__(self):
        return 'HashTagFilter({}{}:{})'.format(
            'exclude' if self.exclude else 'include',
            '' if self.exclusive else ' any',
            self.hashtags)

class DateFilter(TweetFilter):
    def __init__(self, date):
        self.date = parse(date)

class BeforeDateFilter(DateFilter):
    def accepts(self, tweet):
        return self.date > parse(tweet['created_at'])
    def __repr__(self):
        return 'Before({})'.format(self.date.strftime('%Y-%m-%d %H:%M'))

class AfterDateFilter(DateFilter):
    def accepts(self, tweet):
        return self.date < parse(tweet['created_at'])
    def __repr__(self):
        return 'After({})'.format(self.date.strftime('%Y-%m-%d %H:%M'))

class LanguageFilter(TweetFilter):
    def __init__(self, languages):
        self.langs = languages

    def __repr__(self):
        return 'Language({})'.format(self.langs)

    def accepts(self, tweet):
        return tweet['lang'] in self.langs

class SkipUsersFilter(TweetFilter):
    '''Filters out tweets that have any reference to the specified users.  This
    includes tweets from the users, retweets of their tweets, quotes of their
    tweets, or any tweet mentioning them or replying to them.  NOTE that this
    can easily have a broader reach: this filter applies while loading tweets,
    so any tweet that contains references to the specified users will not be
    even loaded. Other users that e.g.  only interact with the skipped users
    will therefore be skipped as well. Use with caution.
    '''

    def __init__(self, skiplist):
        self.skiplist = set(skiplist)

    def __repr__(self):
        return 'SkipUsers({})'.format(self.skiplist)

    def accepts(self, tweet):
        users = {tweet.get_username()}
        if tweet.is_rt():
            users.add(tweet.get_rt_username())
        if tweet.is_quote():
            users.add(tweet.get_qt_username())
        if tweet.is_reply():
            users.add(tweet.get_repl_username())
        for mention in tweet.mentions():
            users.add(mention)
        return not (users & self.skiplist)

class Tweet(dict):

    RT_KEY = 'retweeted_status'
    QUOTE_KEY = 'quoted_status'
    REPLY_KEY = 'in_reply_to_screen_name'

    def is_extended(self):
        # Extended tweets require checking the extended_tweet fields
        # https://developer.twitter.com/en/docs/tweets/data-dictionary/overview/intro-to-tweet-json
        return self['truncated']

    def is_rt(self):
        '''Only counts as a retweet if the original tweet is included.
        If the original tweet was deleted, it won't count
        This is because what we're really interested in are relationships,
        and for this we need the original tweet.'''
        return Tweet.RT_KEY in self.keys()

    def is_quote(self):
        '''Only counts as a quote if the original tweet is included.
        If the original tweet was deleted, it won't count
        This is because what we're really interested in are relationships,
        and for this we need the original tweet.'''
        return Tweet.QUOTE_KEY in self.keys()

    def is_reply(self):
        return self[Tweet.REPLY_KEY] is not None

    def is_simple(self):
        return not (self.is_rt() or self.is_quote() or self.is_reply())

    def get_username(self):
        return self['user']['screen_name']

    def get_rt_username(self):
        return get_username(self[Tweet.RT_KEY])

    def get_qt_username(self):
        return get_username(self[Tweet.QUOTE_KEY])

    def get_repl_username(self):
        return self[Tweet.REPLY_KEY]

    def hashtags(self):
        '''Return a list of hashtags in this tweet, all in lowercase'''
        t = self['extended_tweet'] if self.is_extended() else self
        hashtags = t['entities']['hashtags']
        return [x['text'].lower() for x in hashtags]

    def mentions(self):
        '''Return a list of usernames mentioned by this tweet'''
        t = self['extended_tweet'] if self.is_extended() else self
        mentions = t['entities']['user_mentions']
        return [user['screen_name'] for user in mentions]

    @classmethod
    def csv_header(cls):
        return ['time', 'user', 'id', 'text', 'rt', 'reply', 'quote']

    def to_csv(self):
        if self.is_extended():
            text = self['extended_tweet']['full_text']
        else:
            text = self['text']
        return [
            self['created_at'],
            self.get_username(),
            self['id'],
            text,
            self.is_rt(),
            self.is_reply(),
            self.is_quote()
        ]

def get_username(tweet):
    '''Get the screen name of a tweet's user'''
    return tweet.get_username()


def link(G, a, b, kind=None):
    '''
    Add a link/relation (edge) a->b to graph G.
    The edge's weight represents the number of links a->b
    So, if a link (edge) already exists, we increment its weight by 1
    The weight attribute is always incremented. If "kind" is specified,
    the value of the attribute referenced by "kind" is also incremented by 1
    '''

    # Avoid self-loops
    if a == b: return

    weight = G[a][b]['weight'] + 1 if G.has_edge(a, b) else 1
    G.add_edge(a, b, weight=weight)
    if kind:
        # Also increment the value of the specific kind of relationship
        edge = G[a][b]
        value = edge[kind]+1 if kind in edge.keys() else 1
        G[a][b][kind] = value


def describe_tweets(tweets):
    '''
    Show how many tweets of each type we have
    NOTE: a single tweet can be of multiple types at the same time,
    e.g. a reply that includes a quote. Therefore, the sum of different
    tweet types can be higher than the total of tweets
    '''
    total = len(tweets)
    rts = [t.is_rt() for t in tweets.values()].count(True)
    quotes = [t.is_quote() for t in tweets.values()].count(True)
    replies = [t.is_reply() for t in tweets.values()].count(True)
    simple = [t.is_simple() for t in tweets.values()].count(True)
    print('Tweet types (total {}):'.format(total))
    print('  + {} are retweets ({:.2f}%)'.format(rts, rts/total*100))
    print('  + {} are quotes ({:.2f}%)'.format(quotes, quotes/total*100))
    print('  + {} are replies ({:.2f}%)'.format(replies, replies/total*100))
    print('  + {} are simple ({:.2f}%)'.format(simple, simple/total*100))


def describe_graph(g):
    print('Graph has {} nodes and {} edges.'.format(T.order(), T.size()))
    print('  + Graph density: {:.5f}'.format(nx.density(g)))
    try:
        print('  + Graph diameter: {}'.format(nx.diameter(g)))
        print('  + Graph radius: {}'.format(nx.radius(g)))
    except nx.NetworkXError:
        print("  + Graph isn't strongly connected")


def load_tweets(file_name, filters=[], max=None):
    def create_tweet(obj):
        return Tweet(obj)

    print('Loading {}...'.format(file_name))
    if filters:
        print('  + Filters:')
        for filter in filters:
            print('    - {}'.format(filter))
    loaded = 0
    tweets = {}
    for line in open(file_name, 'r'):
        tweet = json.loads(line, object_hook=create_tweet)
        if all(f.accepts(tweet) for f in filters):
            tweets[tweet['id']] = tweet
            loaded += 1
            if max and loaded >= max:
                break
    print('Loaded a total of {} tweets.'.format(loaded))
    return tweets

def create_graph(tweets, args):
    '''Creates a directed graph from a list of tweets'''
    T = nx.DiGraph()

    # Go through all tweets to create nodes and edges
    for tweet in tweets:
        if args.nosimple and tweet.is_simple():
            continue
        user = tweet.get_username()
        # Make sure each user has their node (noop if it already exists)
        T.add_node(user)
        # Count each interaction separatedly, adding up weight. A tweet of
        # multiple types (e.g. retweet+quote) generates multiple interactions.
        # i.e. an edge with weight > 1 or multiple edges
        if args.rt and tweet.is_rt():
            link(T, user, tweet.get_rt_username(), kind='rt')
        if args.quotes and tweet.is_quote():
            link(T, user, tweet.get_qt_username(), kind='quote')
        if args.replies and tweet.is_reply():
            link(T, user, tweet.get_repl_username(), kind='reply')
        if args.mentions and not tweet.is_rt():
            # Add links for @mentions, but not for retweeted mentions
            for mention in tweet.mentions():
                link(T, user, mention, kind='mention')

    # Remove isolated nodes from the graph (nodes without edges)
    # These can appear due to the flags to avoid relationship types
    T.remove_nodes_from(list(nx.isolates(T)))
    return T

def get_giant_component(G):
    '''Returns the subgraph of the largest connected component of graph G'''
    largest_cc = max(nx.weakly_connected_components(G), key=len)
    return G.subgraph(largest_cc)

def remove_degree_lt(G, mindegree):
    '''Get a subgraph with only nodes with degree >= mindegree'''
    degree = G.degree()
    to_keep = [n for n, d in degree if d >= mindegree]
    return G.subgraph(to_keep)

def louvain(G):
    print("Adding community id to nodes from Louvain's best partition")
    g = G.to_undirected()
    part = community.best_partition(g)
    print("  + Found {} communities".format(len(Counter(part.values()))))
    mod = community.modularity(part, g)
    print("  + Modularity: {:.3f}".format(mod))
    for node in part:
        G.nodes[node]['louvain'] = part[node]

def export_hashtags(tweets, filename):
    '''Write all the hashtags in tweets in a file.
    Each hashtag from each tweet is written in a line in the file.'''
    print('Writing hashtags to {}'.format(filename))
    hashtags = []
    for tweet in tweets:
        hashtags.extend(tweet.hashtags())
    print('  + {} total hashtags'.format(len(hashtags)))
    print('  + Most common:')
    for tag, count in Counter(hashtags).most_common(10):
        print('    - {} {}'.format(tag, count))
    out = open(filename, 'w')
    for hashtag in hashtags:
        out.write(hashtag + "\n")
    out.close()

def export_to_csv(tweets, filename):
    '''Exports the contents of the tweets in CSV format to a file'''
    print(f'Exporting tweets in CSV format to {filename}')
    with open(filename, 'w', newline='') as csvfile:
        csvwriter = csv.writer(csvfile)
        csvwriter.writerow(Tweet.csv_header())
        for tweet in tweets:
            csvwriter.writerow(tweet.to_csv())

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("file_name",
                        help="JSON file with tweets to load, one tweet per line")
    parser.add_argument("--out",
                        help="GraphML output file. Default: twitter.graphml",
                        default="twitter.graphml")
    parser.add_argument("--max",
                        type=int,
                        help="maximum number of tweets to load")
    parser.add_argument("--before",
                        metavar='DATE',
                        help="only tweets before that date")
    parser.add_argument("--after",
                        metavar='DATE',
                        help="only tweets after that date")
    parser.add_argument("--skip",
                        metavar='USER',
                        help="exclude a user from the graph (can be specified multiple times)",
                        action='append')
    parser.add_argument("--exclude",
                        metavar='HASHTAG',
                        help="exclude tweets containing this hashtag (can be specified multiple times)",
                        action='append')
    parser.add_argument("--include",
                        metavar='HASHTAG',
                        help="only load tweets that include this hashtag (can be specified multiple times)",
                        action='append')
    parser.add_argument("--language",
                        help="only tweets in this language (2-letter) (can be specified multiple times)",
                        action='append')
    parser.add_argument("--any",
                        action='store_false',
                        dest='exclusive',
                        help="Allow any of the included hashtags")
    parser.add_argument("--nomentions",
                        action='store_false',
                        dest='mentions',
                        help="Don't add links for metions")
    parser.add_argument("--nort",
                        action='store_false',
                        dest='rt',
                        help="Don't add links for retweets")
    parser.add_argument("--noquotes",
                        action='store_false',
                        dest='quotes',
                        help="Don't add links for quotes")
    parser.add_argument("--noreplies",
                        action='store_false',
                        dest='replies',
                        help="Don't add links for replies")
    parser.add_argument("--nosimple",
                        action='store_true',
                        help="Don't process tweets that are simple (not quote, reply or rt)")
    parser.add_argument("--csv",
                        metavar='FILENAME',
                        help="Export tweets content in CSV format")
    parser.add_argument("--hashtags",
                        metavar='FILENAME',
                        help="Export all hashtags to a file")
    parser.add_argument("--giant",
                        action='store_true',
                        help="Export only the giant component")
    parser.add_argument("--degree",
                        type=int,
                        help="Minimum degree of nodes to consider")
    parser.add_argument("--louvain",
                        action='store_true',
                        help="Add Louvain's best partition as node attribute")
    return parser.parse_args()

def create_filters(args):
    filters = []
    if args.include:
        filters.append(HashTagFilter(hashtags=args.include, exclusive=args.exclusive))
    if args.exclude:
        filters.append(HashTagFilter(hashtags=args.exclude, exclude=True))
    if args.before:
        filters.append(BeforeDateFilter(args.before))
    if args.after:
        filters.append(AfterDateFilter(args.after))
    if args.skip:
        filters.append(SkipUsersFilter(args.skip))
    if args.language:
        filters.append(LanguageFilter(args.language))
    return filters

if __name__ == "__main__":
    args = parse_args()

    tweets = load_tweets(args.file_name, filters=create_filters(args), max=args.max)
    describe_tweets(tweets)

    tweets = tweets.values()
    T = create_graph(tweets, args)
    describe_graph(T)

    if args.degree:
        # Filter out nodes that have a degree below min
        print('Filtering out nodes with degree less than {}'.format(args.degree))
        T = remove_degree_lt(T, args.degree)
        describe_graph(T)

    if args.giant:
        print('Obtaining the giant component')
        T = get_giant_component(T)
        describe_graph(T)

    if args.louvain:
        louvain(T)

    # Popularity Top 5 (users with most inward interactions)
    top5 = sorted(T.nodes, key=lambda node: T.in_degree(node), reverse=True)[:5]
    print('Top 5 users in popularity:\n  - {}'.format('\n  - '.join(top5)))

    if args.hashtags:
        export_hashtags(tweets, args.hashtags)

    if args.csv:
        export_to_csv(tweets, args.csv)

    # Export graph in GraphML
    print('Exporting graph to {}.'.format(args.out))
    nx.write_graphml_lxml(T, args.out)
