#!/usr/bin/env python

import json
import os
import sys
import time
import tweepy
from datetime import datetime


DEFAULT_FILTER = ['kubernetes']

class ConfigError(Exception):
    pass

class StreamFilter:
    def __init__(self):
        keywords = os.getenv('KEYWORDS')
        if not keywords:
            # Default filter
            self.keywords = DEFAULT_FILTER
        else:
            self.keywords = keywords.split()

class TwitterCredentials:
    def __init__(self):
        '''Get configurable values from environment variables'''
        self.consumer_key = os.getenv('CONSUMER_KEY')
        self.consumer_secret = os.getenv('CONSUMER_SECRET')
        self.access_token = os.getenv('ACCESS_TOKEN')
        self.access_secret = os.getenv('ACCESS_SECRET')
        if not (
                self.consumer_key and
                self.consumer_secret and
                self.access_token and
                self.access_secret):
            raise ConfigError("Twitter credentials not set")

class Listener(tweepy.StreamListener):
    '''override tweepy.StreamListener to add logic to on_status and on_error
    and save tweets to an output file
    '''
    def __init__(self, output_path, keywords):
        super(Listener, self).__init__()
        file_fmt = "%Y%m%d-%H%M%S-{}.json".format('-'.join(keywords[:3]))
        file_name = datetime.now().strftime(file_fmt)
        file_path = os.path.join(output_path, file_name)
        self.save_file = open(file_path, 'w', buffering=1)

    def on_status(self, tweet):
        print(tweet.text)
        json.dump(tweet._json, self.save_file, indent=2)

    def on_error(self, status_code):
        print("ERROR: {}".format(status_code), file=sys.stderr)
        if status_code == 420:
            # Limit reached:
            # https://developer.twitter.com/en/docs/basics/rate-limiting
            # A propler exponential backoff strategy should go here
            # but it's a bit overkill for the current assignment.
            # When run within Kubernetes, the exponential backoff
            # is implemented by the RS controller, although it's
            # a bit too fast for twitter's limits. This pause here
            # tries to help the Kubernetes backoff be more in line
            # with twitter's API rate limits
            print("Pausing to recover API rate limit", file=sys.stderr)
            time.sleep(60)
        self.save_file.close()
        # returning False disconnects the stream
        return False

def get_twitter_api(creds):
    print("Authenticating tweepy API...")
    auth = tweepy.OAuthHandler(creds.consumer_key, creds.consumer_secret)
    auth.set_access_token(creds.access_token, creds.access_secret)
    return tweepy.API(auth)

if __name__=='__main__':
    creds = TwitterCredentials()
    stream_filter = StreamFilter()
    output_path = os.getenv('OUTPUT_PATH')
    if not output_path:
        # Default: save to current dir
        output_path = '.'

    api = get_twitter_api(creds)

    listener = Listener(output_path, stream_filter.keywords)
    stream = tweepy.Stream(auth=api.auth, listener=listener)

    print("Listening for tweets matching {}".format(stream_filter.keywords))
    stream.filter(track=stream_filter.keywords)
